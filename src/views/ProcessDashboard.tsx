import React, { useState } from 'react';
import { Box, Divider, Flex, Heading, useToast } from '@chakra-ui/react';
import FileInput from '../components/FileInput';
import { processAPI } from '../api/process';
import { useRefresh } from '../contexts/refresh';
import ProcessTable from './ProcessTable';
import ProcessDailyTable from './ProcessDailyTable';
import ProcessMonthlyTable from './ProcessMonthlyTable';

const ProcessDashboard: React.FC = () => {
    const toast = useToast();
    const { triggerRefresh } = useRefresh();

    const handleFileChange = async (file: File) => {
        try {
            await processAPI.importProcessData(file);
            toast({
                title: 'Success',
                description: 'File was uploaded successfully',
                status: 'success',
                duration: 3000,
                isClosable: true,
            });
            triggerRefresh();
        } catch (error) {
            toast({
                title: 'Error',
                description: `${error}`,
                status: 'error',
                duration: 3000,
                isClosable: true,
            });
        }
    }

    return (
        <Flex direction="column" h="100vh">
            <Flex flex="1">
                <Box flex="1" p="5" h="100%">
                    <Box
                        display="flex"
                        flexDirection="column"
                        alignItems="center"
                        justifyContent="center"
                        width={['100%', '95%', '90%', '80%']}
                        mx="auto"
                        p={[2, 4, 6]}
                    >
                        <FileInput onChange={handleFileChange} inputLabel="Import CSV File"></FileInput>

                        <Heading my={4}>Process Table</Heading>
                        <ProcessTable />

                        <Divider my={6} borderColor="gray.200" />

                        <Heading mb={4}>Process Daily Table</Heading>
                        <ProcessDailyTable />

                        <Divider my={6} borderColor="gray.200" />
                        <Heading mb={4}>Process Monthly Table</Heading>
                        <ProcessMonthlyTable />
                    </Box>
                </Box>
            </Flex>
        </Flex>
    );
};

export default ProcessDashboard;
