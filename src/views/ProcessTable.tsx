import { Box, Table, Thead, Tbody, Tr, Th, Td, TableCaption, useToast, Flex, Tag } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { ProcessData } from "../types/process";
import { processAPI } from "../api/process";
import { useRefresh } from "../contexts/refresh";
import Pagination from "../components/Pagination";
import EmptyTablePlaceholder from "../components/EmptyTablePlaceholder";


const ProcessTable: React.FC = () => {
    const processPerPage = 25;

    const toast = useToast();
    const refresh = useRefresh();
    const [currentPage, setCurrentPage] = useState(1);
    const [processData, setProcessData] = useState<ProcessData>([]);
    const [totalPages, setTotalPages] = useState(0);

    const fetchProcess = async (search?: string) => {
        try {
            const fetchedData = await processAPI.listProcess(currentPage, search);
            setProcessData(fetchedData.data.results);
            setTotalPages(Math.ceil(fetchedData.data.total / processPerPage));
        } catch (error) {
            toast({
                title: 'Error',
                description: `${error}`,
                status: 'error',
                duration: 3000,
                isClosable: true,
            });
        }
    };

    useEffect(() => {
        fetchProcess();
    }, [refresh, currentPage]);

    if (!processData) {
        return (<EmptyTablePlaceholder message="No process data available. Please import a file to view data."></EmptyTablePlaceholder>);
    }
    return (
        <Box>
            <Box height="auto" maxWidth="100%" width="100%" maxHeight="600px" overflowY="auto" overflowX="auto">
                <Table variant="simple" maxW="100%">
                    <Thead style={{
                        position: 'sticky',
                        top: 0,
                        backgroundColor: "white",
                        zIndex: 1000,
                        colorScheme: "custom"
                    }}>
                        <Tr>
                            <Th>Process ID</Th>
                            <Th>Name</Th>
                            <Th>Start</Th>
                            <Th>End</Th>
                            <Th>Duration</Th>
                            <Th>Status</Th>
                            <Th>Message</Th>
                            <Th>Is Outlier</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {processData.map(process => (
                            <Tr key={process.process_id}>
                                <Td>{process.process_id}</Td>
                                <Td>{process.name}</Td>
                                <Td>{process.start}</Td>
                                <Td>{process.end}</Td>
                                <Td>{process.duration}</Td>
                                <Td>
                                    <Tag colorScheme={process.status === "SUCCESS" ? "green" : "red"}>
                                        {process.status}
                                    </Tag>
                                </Td>
                                <Td>{process.message}</Td>
                                <Td>
                                    <Tag colorScheme={process.is_outlier ? "blue" : "red"}>
                                        {process.is_outlier ? "Yes" : "No"}
                                    </Tag>
                                </Td>
                            </Tr>
                        ))}
                    </Tbody>
                </Table>
            </Box>
            <Flex justify="center" mt={4}>
                <Pagination
                    totalPages={totalPages}
                    currentPage={currentPage}
                    onPageChange={(page) => setCurrentPage(page)} />
            </Flex>
        </Box>
    );
}

export default ProcessTable;
