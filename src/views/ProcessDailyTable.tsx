import { Box, Table, Thead, Tbody, Tr, Th, Td, TableCaption, useToast, Flex } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { DailyData, ProcessData } from "../types/process";
import { processAPI } from "../api/process";
import { useRefresh } from "../contexts/refresh";
import Pagination from "../components/Pagination";
import EmptyTablePlaceholder from "../components/EmptyTablePlaceholder";


const ProcessDailyTable: React.FC = () => {
    const processPerPage = 25;

    const toast = useToast();
    const refresh = useRefresh();
    const [currentPage, setCurrentPage] = useState(1);
    const [processDailyData, setProcessDailyData] = useState<DailyData>([]);
    const [totalPages, setTotalPages] = useState(0);

    const fetchProcessDailyData = async (search?: string) => {
        try {
            const fetchedData = await processAPI.listProcessDailyData(currentPage, search);
            setProcessDailyData(fetchedData.data.results);
            setTotalPages(Math.ceil(fetchedData.data.total / processPerPage));
        } catch (error) {
            toast({
                title: 'Error',
                description: `${error}`,
                status: 'error',
                duration: 3000,
                isClosable: true,
            });
        }
    };

    useEffect(() => {
        fetchProcessDailyData();
    }, [refresh, currentPage]);

    if (!processDailyData) {
        return (<EmptyTablePlaceholder message="No process data available. Please import a file to view data."></EmptyTablePlaceholder>);
    }
    return (
        <>
            <Box height="auto" maxWidth="100%" width="100%" maxHeight="600px" overflowY="auto" overflowX="auto">
                <Table variant="simple" maxW="100%">
                    <Thead style={{
                        position: 'sticky',
                        top: 0,
                        backgroundColor: "white",
                        zIndex: 1000,
                        colorScheme: "custom"
                    }}>
                        <Tr>
                            <Th>Name</Th>
                            <Th>Date</Th>
                            <Th>Day Duration</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {processDailyData.map((process, index) => (
                            <Tr key={index}>
                                <Td>{process.name}</Td>
                                <Td>{process.start_date}</Td>
                                <Td>{process.formatted_day_duration}</Td>
                            </Tr>
                        ))}
                    </Tbody>
                </Table>
            </Box>
            <Flex justify="center" mt={4}>
                <Pagination
                    totalPages={totalPages}
                    currentPage={currentPage}
                    onPageChange={(page) => setCurrentPage(page)} />
            </Flex>
        </>
    );
}

export default ProcessDailyTable;
