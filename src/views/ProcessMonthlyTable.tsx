import { Box, Table, Thead, Tbody, Tr, Th, Td, TableCaption, useToast, Flex } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { MonthlyData } from "../types/process";
import { processAPI } from "../api/process";
import { useRefresh } from "../contexts/refresh";
import Pagination from "../components/Pagination";
import EmptyTablePlaceholder from "../components/EmptyTablePlaceholder";


const ProcessMonthlyTable: React.FC = () => {
    const processPerPage = 25;

    const toast = useToast();
    const refresh = useRefresh();
    const [currentPage, setCurrentPage] = useState(1);
    const [processMonthlyData, setProcessMonthlyData] = useState<MonthlyData>([]);
    const [totalPages, setTotalPages] = useState(0);

    const fetchProcessMonthlyData = async (search?: string) => {
        try {
            const fetchedData = await processAPI.listProcessMonthlyData(currentPage, search);
            setProcessMonthlyData(fetchedData.data.results);
            setTotalPages(Math.ceil(fetchedData.data.total / processPerPage));
        } catch (error) {
            toast({
                title: 'Error',
                description: `${error}`,
                status: 'error',
                duration: 3000,
                isClosable: true,
            });
        }
    };

    useEffect(() => {
        fetchProcessMonthlyData();
    }, [refresh, currentPage]);

    if (!processMonthlyData) {
        return (<EmptyTablePlaceholder message="No process data available. Please import a file to view data."></EmptyTablePlaceholder>);
    }
    return (
        <>
            <Box height="auto" maxWidth="100%" width="100%" maxHeight="600px" overflowY="auto" overflowX="auto">
                <Table variant="simple" maxW="100%">
                    <TableCaption>Process Monthly Data</TableCaption>
                    <Thead style={{
                        position: 'sticky',
                        top: 0,
                        backgroundColor: "white",
                        zIndex: 1000,
                        colorScheme: "custom"
                    }}>
                        <Tr>
                            <Th>Name</Th>
                            <Th>Total of calls</Th>
                            <Th># of Error calls</Th>
                            <Th>Error rate</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {processMonthlyData.map((process, index) => (
                            <Tr key={index}>
                                <Td>{process.name}</Td>
                                <Td>{process.total_processes}</Td>
                                <Td>{process.error_processes}</Td>
                                <Td>{process.error_rate}</Td>
                            </Tr>
                        ))}
                    </Tbody>
                </Table>
            </Box>
            <Flex justify="center" mt={4}>
                <Pagination
                    totalPages={totalPages}
                    currentPage={currentPage}
                    onPageChange={(page) => setCurrentPage(page)} />
            </Flex>
        </>
    );
}

export default ProcessMonthlyTable;
