export type Process = {
    process_id: number;
    name: string;
    start: string;
    end: string;
    duration: number;
    status: string;
    message: string;
    is_outlier?: boolean;
};

export type ProcessDaily = {
    name: string;
    day_duration?: number;
    formatted_day_duration?: string;
    start_date: string;
}

export type ProcessMonthly = {
    name: string;
    error_rate: number;
    total_processes?: number;
    error_processes?: number;
}


export type ProcessData = Process[];
export type DailyData = ProcessDaily[];
export type MonthlyData = ProcessMonthly[];
