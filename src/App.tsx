// src/App.tsx

import React from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import Routes from './routes';
import './i18n';

const App: React.FC = () => {
  return (
    <Router>
      <div>
        <main>
          <Routes />
        </main>
      </div>
    </Router>
  );
}

export default App;
