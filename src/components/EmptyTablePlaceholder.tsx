import { Box, Text } from "@chakra-ui/react";

interface EmptyTablePlaceholderType {
    message: string;
}

const EmptyTablePlaceholder: React.FC<EmptyTablePlaceholderType> = ({ message }) => {
    return (
        <Box
            display="flex"
            alignItems="center"
            justifyContent="center"
            p={5}
            width="100%"
            bg="gray.100"
            border="2px dashed gray"
            borderRadius="md"
            my={3}
        >
            <Text fontWeight="bold">{message}</Text>
        </Box>
    )
};


export default EmptyTablePlaceholder;
