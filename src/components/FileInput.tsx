import React, { useState } from 'react';
import { Button, Input, Box, FormControl, FormLabel } from '@chakra-ui/react';

interface FileInputProps {
    label?: string;
    inputLabel?: string;
    onChange?: (file: File) => void;
}

const FileInput: React.FC<FileInputProps> = ({ label, inputLabel = "Select a file", onChange }) => {
    const [selectedFile, setSelectedFile] = useState<string | null>(null);

    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const files = e.target.files;
        if (files && files.length > 0) {
            setSelectedFile(files[0].name);
            if (onChange) {
                onChange(files[0]);
            }
        }
    };

    return (
        <>
            {label && (<FormLabel>{label}</FormLabel>)}
            <Input type="file" onChange={handleFileChange} hidden id="file-input" />
            <Button as="label" htmlFor="file-input" size="md">
                {selectedFile ? selectedFile : inputLabel}
            </Button>
        </>
    );
}

export default FileInput;
