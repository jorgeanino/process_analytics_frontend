import React from 'react';
import { Spinner, Container, Center, defineStyle, defineStyleConfig } from '@chakra-ui/react'


const xxl = defineStyle({
    height: 500,
    width: 500,
});

export const spinnerTheme = defineStyleConfig({
    sizes: { xxl },
})

// Now we can use the new `xxl` size
const Dashboard: React.FC = () => {

    return (
        <Container>
            <Center>
                <Spinner size="xxl" >...</Spinner>
            </Center>
        </Container>
    );
};

export default Dashboard;
