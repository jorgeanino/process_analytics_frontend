import {
    Box,
    Button,
    HStack,
    useBreakpointValue
} from "@chakra-ui/react";
import React from "react";

interface PaginationProps {
    totalPages: number;
    currentPage: number;
    onPageChange: (page: number) => void;
}

const Pagination: React.FC<PaginationProps> = ({ totalPages, currentPage, onPageChange }) => {
    const numberOfPagesToShow = useBreakpointValue({ base: 3, md: 5, lg: 7 }) || 5;
    const halfOfPagesToShow = Math.floor(numberOfPagesToShow / 2);

    let startPage = Math.max(currentPage - halfOfPagesToShow, 1);
    let endPage = Math.min(currentPage + halfOfPagesToShow, totalPages);

    if (totalPages <= numberOfPagesToShow) {
        startPage = 1;
        endPage = totalPages;
    }

    const pages = Array.from({ length: endPage - startPage + 1 }, (_, i) => startPage + i);

    if (totalPages === 1) {
        return (
            <HStack>
                <Button disabled={true}>Previous</Button>
                <Box p={2} bg="blue.500" color="white">1</Box>
                <Button disabled={true}>Next</Button>
            </HStack>
        );
    }

    return (
        <HStack spacing={4} wrap="wrap">
            <Button
                disabled={currentPage === 1}
                onClick={() => currentPage > 1 && onPageChange(currentPage - 1)}
            >
                Previous
            </Button>

            {startPage > 1 && (
                <>
                    <Box p={2} onClick={() => onPageChange(1)} cursor="pointer">1</Box>
                    <Box p={2}>...</Box>
                </>
            )}

            {pages.map((page) => (
                <Box
                    key={page}
                    p={2}
                    onClick={() => onPageChange(page)}
                    cursor="pointer"
                    bg={page === currentPage ? "blue.500" : "transparent"}
                    color={page === currentPage ? "white" : "black"}
                >
                    {page}
                </Box>
            ))}

            {endPage < totalPages && (
                <>
                    <Box p={2}>...</Box>
                    <Box p={2} onClick={() => onPageChange(totalPages)} cursor="pointer">{totalPages}</Box>
                </>
            )}

            <Button
                disabled={currentPage === totalPages}
                onClick={() => currentPage < totalPages && onPageChange(currentPage + 1)}
            >
                Next
            </Button>
        </HStack>
    );
};

export default Pagination;
