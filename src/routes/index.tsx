import { useRoutes } from 'react-router-dom';
import ProcessDashboard from '../views/ProcessDashboard';


export default function Router() {
  return useRoutes([
    { path: '/', element: <ProcessDashboard />, index: true },
  ]);
}
