import { createContext, useContext, useCallback, useState, ReactNode } from "react";

type RefreshContextType = {
    triggerRefresh: () => void;
};

const TableRefreshContext = createContext<RefreshContextType | undefined>(undefined);

interface RefreshProviderProps {
    children: ReactNode;
}

export const useRefresh = (): RefreshContextType => {
    const context = useContext(TableRefreshContext);
    if (!context) {
        throw new Error("useRefresh must be used within a RefreshProvider");
    }
    return context;
};

export const TableRefreshProvider: React.FC<RefreshProviderProps> = ({ children }) => {
    const [refreshKey, setRefreshKey] = useState(false);

    const triggerRefresh = useCallback(() => {
        console.log("trigger")
        setRefreshKey(prev => !prev);
    }, []);

    return (
        <TableRefreshContext.Provider value={{ triggerRefresh }}>
            {children}
        </TableRefreshContext.Provider>
    );
};
