import axios from 'axios';

const BASE_URL = `${process.env.REACT_APP_BACKEND_BASE_URL}/api/v1`
const PROCESS_URL = `${BASE_URL}/process`

const api = axios.create({
    baseURL: BASE_URL,
});

const listProcess = async (page?: number, search?: string) => {
    const url = new URL(`${PROCESS_URL}/`);

    if (page) {
        url.searchParams.set('page', page.toString());
    }

    if (search) {
        url.searchParams.set('search', search);
    }

    const response = await api.get(url.toString());
    return response;
};

const listProcessDailyData = async (page?: number, search?: string) => {
    const url = new URL(`${PROCESS_URL}/daily/`);

    if (page) {
        url.searchParams.set('page', page.toString());
    }

    if (search) {
        url.searchParams.set('search', search);
    }

    const response = await api.get(url.toString());
    return response;
};


const listProcessMonthlyData = async (page?: number, search?: string) => {
    const url = new URL(`${PROCESS_URL}/monthly/`);

    if (page) {
        url.searchParams.set('page', page.toString());
    }

    if (search) {
        url.searchParams.set('search', search);
    }

    const response = await api.get(url.toString());
    return response;
};

const importProcessData = async (file: File) => {
    const formData = new FormData();
    formData.append('file', file);

    const response = await api.post(`${PROCESS_URL}/import/`, formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
        },
    });
    return response;
};

export const processAPI = {
    listProcess,
    listProcessDailyData,
    listProcessMonthlyData,
    importProcessData
}
